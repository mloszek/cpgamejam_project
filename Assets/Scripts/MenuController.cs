﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MenuController : MonoBehaviour
{
    [SerializeField]
    private Text highscoreText;
    [SerializeField]
    private string firstLevel;

    public void Begin()
    {
        ScoreHolder.SceneIndex = 1;
        SceneManager.LoadScene(1, LoadSceneMode.Single);
    }

    public void Exit()
    {
        Application.Quit();
    }

    private void Start()
    {
        highscoreText.text = string.Empty;//string.Format("HIGH SCORE\n{0}", PlayerPrefs.HasKey("highscore") ? PlayerPrefs.GetInt("highscore").ToString() : "0");
    }
}