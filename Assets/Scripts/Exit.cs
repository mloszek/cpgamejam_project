﻿using UnityEngine;
using System.Collections;

public class Exit : MonoBehaviour
{
    [SerializeField]
    private GameController gameController;

    private Collider collider;

    void Start()
    {
        collider = GetComponent<SphereCollider>();
    }

    private void OnTriggerEnter(Collider other)
    {
        Rigidbody ballRigidbody = other.GetComponent<Rigidbody>();
        ballRigidbody.velocity = Vector3.zero;
        ballRigidbody.angularVelocity = Vector3.zero;
        ballRigidbody.isKinematic = true;
        other.transform.position = gameObject.transform.position;
        GetComponent<AudioSource>().Play();
        other.gameObject.GetComponent<Animator>().SetTrigger("fadeOut");
        StartCoroutine(ScaleOutBall(other.gameObject, 1f));        
    }

    private IEnumerator ScaleOutBall(GameObject ball, float fadeTime)
    {
        yield return new WaitForSeconds(fadeTime);

        ball.gameObject.SetActive(false);
        gameController.GoToNextScene();
    }
}