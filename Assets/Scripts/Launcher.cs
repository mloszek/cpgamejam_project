﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Launcher : MonoBehaviour
{
    [SerializeField]
    private GameObject ball;
    [SerializeField]
    private Rigidbody ballBody;
    [SerializeField]
    private LineRenderer lineRenderer;
    [SerializeField]
    private Camera camera;
    [SerializeField]
    private float shootForce;
    [SerializeField]
    private ForceMode forceMode;

    private Vector3 tempPosition;
    private bool canShoot = true;

    void Update()
    {
        if (canShoot)
        {
            if (Input.GetMouseButton(0))
            {
                tempPosition = camera.ScreenToWorldPoint(Input.mousePosition);

                lineRenderer.SetPosition(1, tempPosition);
                lineRenderer.enabled = true;
            }
            else
            {
                lineRenderer.enabled = false;
            }

            if (Input.GetMouseButtonUp(0))
            {
                LaunchBall();
            }
        }
    }

    private void LaunchBall()
    {
        GetComponent<AudioSource>().Play();
        canShoot = false;
        ballBody.useGravity = true;
        tempPosition = (ball.transform.position - tempPosition).normalized;
        tempPosition.z = 0;
        ball.GetComponent<Rigidbody>().AddForce(tempPosition * shootForce, forceMode);
    }
}
