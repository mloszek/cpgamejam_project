﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Obstacle : MonoBehaviour
{
    [SerializeField]
    private Color color;
    [SerializeField]
    private GameObject explosion;

    private GameController gameController;

    void Start()
    {
        gameObject.GetComponent<MeshRenderer>().material.color = color;
        gameController = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameController>();
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.GetComponent<MeshRenderer>().material.color != gameObject.GetComponent<MeshRenderer>().material.color)
        {
            Destroy(collision.gameObject);
            Destroy(Instantiate(explosion, collision.gameObject.transform.position, Quaternion.identity), 2f);
            gameController.GameOver();
        }
        else
        {
            GetComponent<AudioSource>().Play();
        }
    }
}
