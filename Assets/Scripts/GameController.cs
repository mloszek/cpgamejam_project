﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameController : MonoBehaviour
{
    [SerializeField]
    private IngameCanvasController canvasController;

    public void GameOver()
    {
        canvasController.SetGameOverScreen();
    }

    public void ReloadScene()
    {
        var scene = SceneManager.GetActiveScene();
        SceneManager.LoadScene(scene.name, LoadSceneMode.Single);
    }

    public void GoToNextScene()
    {
        ScoreHolder.SceneIndex += 1;
        SceneManager.LoadScene(ScoreHolder.SceneIndex, LoadSceneMode.Single);
    }
}
