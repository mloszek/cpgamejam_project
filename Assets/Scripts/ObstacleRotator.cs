﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObstacleRotator : MonoBehaviour
{
    [SerializeField]
    private float speed;

    void Update()
    {
        gameObject.transform.Rotate(new Vector3(0, 0, speed * Time.deltaTime));        
    }
}
