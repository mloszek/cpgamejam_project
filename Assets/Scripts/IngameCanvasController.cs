﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class IngameCanvasController : MonoBehaviour
{
    [SerializeField]
    private Text scoreText;
    [SerializeField]
    private GameObject restartButton, loseText, highscoreText;        

    public void RestartLevel()
    {
        var scene = SceneManager.GetActiveScene();
        SceneManager.LoadScene(scene.name, LoadSceneMode.Single);
    }

    public void SetGameOverScreen()
    {
        //CheckHighScore();
        restartButton.SetActive(true);
        //loseText.SetActive(true);
    }

    private void Start()
    {
        scoreText.text = string.Empty;//string.Format("SCORE\n{0}", ScoreHolder.SceneIndex - 1);
        restartButton.GetComponent<Button>().onClick.AddListener(RestartLevel);
        restartButton.SetActive(false);
        loseText.SetActive(false);
        highscoreText.SetActive(false);
    }

    private void CheckHighScore()
    {
        if (PlayerPrefs.HasKey("highscore"))
        {
            if (PlayerPrefs.GetInt("highscore") >= ScoreHolder.SceneIndex - 1)
                return;
            else
            {
                highscoreText.SetActive(true);
                PlayerPrefs.SetInt("highscore", ScoreHolder.SceneIndex - 1);
            }
        }
        else
        {
            highscoreText.SetActive(true);
            PlayerPrefs.SetInt("highscore", ScoreHolder.SceneIndex - 1);
        }
    }
}
