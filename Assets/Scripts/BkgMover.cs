﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BkgMover : MonoBehaviour
{
    [SerializeField]
    private Material material;
    [SerializeField]
    private float speed;

    private Vector2 offset;

    private void Start()
    {
        offset = material.mainTextureOffset;
        material.mainTextureOffset = Vector2.zero;
    }

    private void Update()
    {
        offset.x -= speed / 100;
        offset.y += speed / 80;
        material.mainTextureOffset = offset;
    }
}
