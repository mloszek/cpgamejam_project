﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Portal : MonoBehaviour
{
    [SerializeField]
    private GameObject exitPortal;
    [SerializeField]
    private Color color;

    private Collider collider;

    void Start()
    {
        gameObject.GetComponent<MeshRenderer>().material.SetColor("_HoloColor", color);
        exitPortal.GetComponent<MeshRenderer>().material.SetColor("_HoloColor", color);
        collider = GetComponent<SphereCollider>();        
    }

    private void OnTriggerEnter(Collider other)
    {
        GetComponent<AudioSource>().Play();
        other.transform.position = exitPortal.gameObject.transform.position;
        other.GetComponent<MeshRenderer>().material.color = gameObject.GetComponent<MeshRenderer>().material.GetColor("_HoloColor");
    }
}
