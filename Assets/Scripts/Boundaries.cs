﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Boundaries : MonoBehaviour
{
    [SerializeField]
    private GameController gameController;

    private Collider collider;

    private void Start()
    {
        collider = GetComponent<BoxCollider>();
    }

    private void OnCollisionEnter(Collision collision)
    {
        Destroy(collision.gameObject);
        gameController.GameOver();
    }
}
