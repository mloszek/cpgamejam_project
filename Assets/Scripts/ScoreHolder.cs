﻿using UnityEngine.SceneManagement;

public static class ScoreHolder
{
    private static int sceneIndex;

    public static int SceneIndex
    {
        get { return sceneIndex; }
        set { sceneIndex = value >= SceneManager.sceneCountInBuildSettings ? sceneIndex : value; }
    }
}
